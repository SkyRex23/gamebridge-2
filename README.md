# GameBridge [EN] documentation
# Bachelor project 

Gamebridge V2 is a PWA (App & Website) allows you to look for players with criteria defined for some game sessions !
Here are some of them : 
- Game (Call of Duty, Apex, CS:GO...)
- Gamemode (Zombie, Multiplayer, League, Ranked...)
- Platform (PS4, PS5, XONE, XSERIES, PC, Switch)
- Goal (Fun, Competitive / Ranked, Record)
- Date
- Description

## Technologies used

- **Frontend** : Ionic + Angular (HTML5, SCSS, TypeScript)
- **Backend** : Firebase (Cloud Firestore) with custom Functions 
- **Database** : Firebase Firestore (Auth + Firestore Database) for authentification and store the data

## Used versions

- nodeJS : 14.15.4
- npm : 6.14.10
- Ionic : 6.16.3
- Angular : 10.2.0

## Installation 

### Prerequired
- NodeJS (npm) ___via nvm___ : https://ionicframework.com/docs/developing/tips#resolving-permission-errors - Option 1
- Ionic : https://ionicframework.com/docs/intro/cli `npm install -g @ionic/cli`

### Install dependencies
```bash
npm install
```


### Start the Frontend
```bash
ionic serve
```

### Start the Backend

All the function are deployed on Firebase and the frontend call directly from firebase links. So no need to run a backend service. 
The functions are in the  : ```functions/src/index.ts``` folder.

### /!\ THE FOLLOWING IS NOT NECESSARY - JUST IF YOU NEED TO TEST THE FUNCTION LOCALLY /!\

- 1st :  Be sure that all the dependencies are installed by running a ```npm install``` in the ```functions``` folder the command:

Then execute : ```npm run build```

- 2nd: Be sure that the links in code redirect to a localhost url. By default, they redirect to the final Firebase link.
- 3rd: You must be authenticated with your Firebase account. If you are not in the project you can't access to it. 

Execute this command to log in: 
```bash
firebase login
```
It will open your browser, normal behaviour. Use the credentials used to connect


Finally, you can run the command to emulate the functions locally and test it: 
```bash
firebase emulators:start
```

Once the function tested locally you can deploy your function by running :
```bash
firebase deploy --only functions
```

### Author
@SkyRex23 / André Guerreiro





