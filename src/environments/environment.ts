// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyCFfMfL4GWwgyymSz3AkHm_oR7fy51sI54',
    authDomain: 'gamebridge-app.firebaseapp.com',
    databaseURL: 'https://gamebridge-app.firebaseio.com',
    projectId: 'gamebridge-app',
    storageBucket: 'gamebridge-app.appspot.com',
    messagingSenderId: '1049229261050',
    appId: '1:1049229261050:web:df13de9377dd96b2b5b143',
  }
}

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
