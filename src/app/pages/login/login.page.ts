import { Component, OnInit } from '@angular/core'
import { ModalController, AlertController, LoadingController, IonRouterOutlet} from '@ionic/angular'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'

import { AuthService } from '../../services/auth/auth.service'

import { ModalBaseComponent} from '../../components/modal-base/modal-base.component'
import { ResetPwPage } from '../reset-pw/reset-pw.page'
import { Router } from '@angular/router'
import {AngularFirestore} from '@angular/fire/firestore'

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})

export class LoginPage implements OnInit {
  loginForm: FormGroup
  showPw = false
  resetPwPage = ResetPwPage

  constructor(
      private modalCtrl: ModalController,
      private routerOutlet: IonRouterOutlet,
      private fb: FormBuilder,
      private auth: AuthService,
      private afs: AngularFirestore,
      private alertController: AlertController,
      private loadingController: LoadingController,
      private router: Router
  ) {}

  ngOnInit() {
    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    })
  }

  /**
   * Sign in to the app with email and password and redirect
   */
  async signIn() {
    const loading = await this.loadingController.create()
    await loading.present()

    this.auth
        .signIn(this.loginForm.value)
        .then( () => {
              loading.dismiss()
              this.router.navigateByUrl('/tabs/homepage')
            },
            async (err) => {
              await loading.dismiss()
              const alert = await this.alertController.create({
                header: ':(',
                message: 'Il semblerait que tes données ne sont pas correctes ou inexistantes...',
                buttons: ['OK'],
              })
              await alert.present()
            }
        )
  }

  async openResetPw() {
    const modal = await this.modalCtrl.create({
      component: ModalBaseComponent,
      presentingElement: this.routerOutlet.nativeEl,
      swipeToClose: true,
      componentProps: {
        rootPage: ResetPwPage,
      }
    })
    await modal.present()
  }

  get email() {
    return this.loginForm.get('email')
  }

  get password() {
    return this.loginForm.get('password')
  }
}
