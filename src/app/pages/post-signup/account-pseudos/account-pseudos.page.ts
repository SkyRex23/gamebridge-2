import { Component, OnInit } from '@angular/core'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { AlertController, LoadingController, ModalController, ToastController } from '@ionic/angular'
import { Router } from '@angular/router'
import { AuthService } from '../../../services/auth/auth.service'
import { HttpClient } from '@angular/common/http'

@Component({
    selector: 'app-account-pseudos',
    templateUrl: './account-pseudos.page.html',
    styleUrls: ['./account-pseudos.page.scss'],
})

export class AccountPseudosPage implements OnInit {
    pseudoForm: FormGroup

    constructor(
        private modalCtrl: ModalController,
        private fb: FormBuilder,
        private auth: AuthService,
        private loadingController: LoadingController,
        private alertController: AlertController,
        private router: Router,
        private http: HttpClient,
        private toastController: ToastController
    ) {
    }

    ngOnInit() {
        this.pseudoForm = this.fb.group({
            username: [
                '', [
                    Validators.required,
                    Validators.minLength(3),
                    Validators.maxLength(12)
            ], ]
        })
    }

    async sendPseudo(): Promise<any> {
        const loading = await this.loadingController.create()
        await loading.present()


        const currentUserUid = await this.auth.getCurrentUser()
            .then(res => {
                return res.uid
            })
            .catch(err => console.log(err))

        // Need the current user uid for the following call
        this.http.post('https://us-central1-gamebridge-app.cloudfunctions.net/updateUsername', {
            username: this.pseudoForm.value.username,
            playerUid: currentUserUid,
            responseType: 'json',
            headers: {'Access-Control-Allow-Origin': '*'}
        })
            .toPromise()
            .then(async () => {
                await this.presentToast(`${this.pseudoForm.value.username} sera désormais ton nouveau pseudo !`)
                await loading.dismiss()
                if (await this.modalCtrl.getTop() !== undefined) {
                    await this.modalCtrl.dismiss()
                }
                await this.router.navigateByUrl('tabs/homepage')
            })
            .catch(async err => {
                await this.presentToast(`${this.pseudoForm.value.username} sera désormais ton nouveau pseudo !`)
                // Check if modal is present or not (called in modal from page Welcome)
                await loading.dismiss()
                if (await this.modalCtrl.getTop() !== undefined) {
                    await this.modalCtrl.dismiss()
                }
                await this.router.navigateByUrl('tabs/homepage')
            })
    }

    async presentToast(msg) {
        const toast = await this.toastController.create({
            mode: 'ios',
            color: 'dark',
            message: msg,
            duration: 2000
        })
        await toast.present()
    }
}
