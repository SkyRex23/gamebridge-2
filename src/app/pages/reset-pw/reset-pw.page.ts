import {LoadingController, AlertController, IonNav, ModalController } from '@ionic/angular'
import { Component, OnInit } from '@angular/core'
import { AuthService } from '../../services/auth/auth.service'

@Component({
  selector: 'app-reset-pw',
  templateUrl: './reset-pw.page.html',
  styleUrls: ['./reset-pw.page.scss'],
})

export class ResetPwPage implements OnInit {
  email = ''

  constructor(
      private modalCtrl: ModalController,
      private auth: AuthService,
      private loadingController: LoadingController,
      private alertController: AlertController,
      private nav: IonNav,
  ) {}

  ngOnInit() {}

  async sendResetEmail() {
    const loading = await this.loadingController.create()
    await loading.present()
    this.auth.resetPw(this.email).then(
        async res => {
          await loading.dismiss()
          const alert = await this.alertController.create({
            header: 'Succès',
            message: 'Un email t\'a été envoyé !',
            buttons: [
              {
                text: 'OK',
                handler: () => {
                  this.nav.pop()
                }
              }
            ]
          })
          await alert.present()
        },
        async err => {
          await loading.dismiss()
          const alert = await this.alertController.create({
            header: ':(',
            message: err.message,
            buttons: ['OK']
          })
          await alert.present()
        }
    )
  }

  close() {
    this.modalCtrl.dismiss().then(() => {console.log('Modal closed')})
  }
}
