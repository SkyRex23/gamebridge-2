import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { SessionGamePage } from './session-game.page'

const routes: Routes = [
  {
    path: '',
    component: SessionGamePage
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SessionGamePageRoutingModule {}
