import {NgModule} from '@angular/core'
import {CommonModule} from '@angular/common'
import {FormsModule, ReactiveFormsModule} from '@angular/forms'
import {IonicModule} from '@ionic/angular'
import {SessionGamePageRoutingModule} from './session-game-routing.module'
import {SessionGamePage} from './session-game.page'
import {PipesModule} from '../../../pipes/pipes.module'

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        SessionGamePageRoutingModule,
        ReactiveFormsModule,
        PipesModule
    ],
  declarations: [SessionGamePage]
})
export class SessionGamePageModule {}
