import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { IonicModule } from '@ionic/angular'

import { SessionGamePage } from './session-game.page'

describe('SessionGamePage', () => {
  let component: SessionGamePage
  let fixture: ComponentFixture<SessionGamePage>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SessionGamePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents()

    fixture = TestBed.createComponent(SessionGamePage)
    component = fixture.componentInstance
    fixture.detectChanges()
  }))

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
