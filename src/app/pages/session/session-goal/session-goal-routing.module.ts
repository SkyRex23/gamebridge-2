import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { SessionGoalPage } from './session-goal.page'

const routes: Routes = [
  {
    path: '',
    component: SessionGoalPage
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SessionGoalPageRoutingModule {}
