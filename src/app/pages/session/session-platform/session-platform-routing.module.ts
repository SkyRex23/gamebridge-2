import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { SessionPlatformPage } from './session-platform.page'

const routes: Routes = [
  {
    path: '',
    component: SessionPlatformPage
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SessionPlatformPageRoutingModule {}
