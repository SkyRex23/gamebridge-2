import { Component, OnInit } from '@angular/core'
import { SessionService } from '../../../services/session/session.service'
import {AlertController, LoadingController, ToastController} from '@ionic/angular'
import { AuthService } from '../../../services/auth/auth.service'
import { Session } from '../../../interfaces/Session'
import { HttpClient } from '@angular/common/http'
import { Plugins } from '@capacitor/core'
const  { Storage } = Plugins
import { Router } from '@angular/router'



@Component({
    selector: 'app-session-available',
    templateUrl: './session-available.page.html',
    styleUrls: ['./session-available.page.scss'],
})

export class SessionAvailablePage implements OnInit {

    sessionWithoutPlayerInIt: Session[] = []
    sessions: Session[] = []
    sessionsExists = false

    constructor(
        private loadingController: LoadingController,
        private alertController: AlertController,
        private sessionService: SessionService,
        private auth: AuthService,
        private router: Router,
        private http: HttpClient,
        private toastController: ToastController
    ) { }

    async ngOnInit() {
    }

    async ionViewWillEnter() {
        const loading = await this.loadingController.create()
        await loading.present()

        await this.compareData()
            .then(() => this.filterSessions())
            .then(() => this.finalRedirection(loading))
            .then(() => loading.dismiss())
            .catch(err => console.log(err))
    }

    /**
     * Will send the parameters (game, mode and goal) and returns the list of the sessions with the same preferences
     * It will return also the session where the player is in it because there no "where" condition for array not containing a given value
     * If a session or many exist(s), a message will advise the user that sessions with the same preferences already exists.
     * The use can scroll and see the different sessions but a message to create the session will be displayed at the bottom of the screen.
     *
     * If no session exist with the same preferences, a message will warn the user and the session will be created anyway, then
     * The user is redirected to the homepage
     */
    async compareData(): Promise<[] | Session[]> {
        return new Promise(async resolve => {
            const sessionToBeSent = await Storage.get({key: 'session'})
            const session: Session = JSON.parse(sessionToBeSent.value)

            await this.http
                .post('https://us-central1-gamebridge-app.cloudfunctions.net/getSessionsByPreferences', {
                    game: session.game,
                    mode: session.mode,
                    goal: session.goal,
                }, {
                    responseType: 'json',
                    headers: {'Access-Control-Allow-Origin': '*'}
                })
                .subscribe((data: Session[] | []) => {
                        this.sessions = data
                        this.sessionsExists = true
                        resolve(data)
                })
        })
    }

    /**
     * Check if user is in the Session
     */
    async filterSessions() {
        return new Promise(async resolve => {
            const userUid = await this.auth.getCurrentUser().then((user) => user.uid)

            for (const session of this.sessions) {
                if (!session.listPlayers.includes(userUid)) {
                    this.sessionWithoutPlayerInIt.push(session)
                }
            }
            resolve(this.sessionWithoutPlayerInIt)
        })

    }

    async finalRedirection(loading) {
        return new Promise(async resolve => {
            const sessionToBeSent = await Storage.get({key: 'session'})
            const session: Session = JSON.parse(sessionToBeSent.value)

            if (this.sessionWithoutPlayerInIt.length <= 0) {
                await this.sendSession(session)
                loading.dismiss()
                await this.presentAlert()
                resolve()
            } else {
                loading.dismiss()
            }

        })
    }

    async sendSession(session: Session): Promise<void>{
        await this.http
            .post('https://us-central1-gamebridge-app.cloudfunctions.net/createSession', {
                game: session.game,
                gameId: session.gameId,
                mode: session.mode,
                goal: session.goal,
                platform: session.platform,
                date: session.date,
                description: session.description,
                createdAt: session.createdAt,
                createdBy: session.createdBy,
                expiredAt: session.expiredAt,
                listPlayers: session.listPlayers
            }, {
                responseType: 'json',
                headers: {'Access-Control-Allow-Origin': '*'}})
            .subscribe(() => {console.log('Session created properly')})
    }

    async presentAlert() {
        const alert = await this.alertController.create({
            cssClass: '',
            header: 'Session crée !',
            message: 'Ta session est unique et a été directement crée. Tu vas être redirigé vers la page d\'accueil',
            buttons: [
                {
                    text: 'OK',
                    handler: async () => {
                        await this.router.navigateByUrl('tabs/homepage')
                    }
                }
            ]
        })

        await alert.present()
    }

    async joinSession(session: Session) {
        let playerToAdd: string = null

        await this.auth.getCurrentUser().then(user => {
            playerToAdd = user.uid
        })

        this.http
            .post('https://us-central1-gamebridge-app.cloudfunctions.net/addPlayerToSession', {
                sessionUid: session.uid,
                playerToAdd
            }, {
                responseType: 'json',
                headers: {'Access-Control-Allow-Origin': '*'}
            })
            .subscribe(() => {
                console.log('Session created properly')
                this.presentToast('Succès !')
                this.router.navigateByUrl('tabs/homepage')
            })
    }

    async createSessionAnyway() {
        const sessionToBeSent = await Storage.get({key: 'session'})
        const session: Session = JSON.parse(sessionToBeSent.value)

        await this.sendSession(session)
            .then(() => {
                this.presentToast('Session crée avec succès!')
                this.router.navigateByUrl('tabs/homepage')
            })
            .catch(err => console.log(err))
    }

    async presentToast(msg) {
        const toast = await this.toastController.create({
            mode: 'ios',
            color: 'dark',
            message: msg,
            duration: 2000
        })
        await toast.present()
    }

}
