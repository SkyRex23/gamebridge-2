import { Component, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { Router } from '@angular/router'
import { SessionService } from '../../../services/session/session.service'
import { AlertController, LoadingController} from '@ionic/angular'
import { Plugins } from '@capacitor/core'
import { AuthService } from '../../../services/auth/auth.service'
import { HttpClient } from '@angular/common/http'
const  { Storage } = Plugins
import { Session } from 'src/app/interfaces/Session'


@Component({
  selector: 'app-session-description',
  templateUrl: './session-description.page.html',
  styleUrls: ['./session-description.page.scss'],
})
export class SessionDescriptionPage implements OnInit {

  descriptionForm: FormGroup
  choices: any[] = []

  constructor(
      private fb: FormBuilder,
      private http: HttpClient,
      private auth: AuthService,
      private sessionAuth: SessionService,
      private alertController: AlertController,
      private router: Router,
      private loadingController: LoadingController,
  ) { }

  ngOnInit() {
    this.descriptionForm = this.fb.group({
      description: [null, [Validators.required]],
    })
  }
  async ionViewWillEnter() {
    // Show loading
    const loading = await this.loadingController.create()
    await loading.present()

    this.getSessionFromStorage().then(() => {
      loading.dismiss()
    })
  }

  async saveAndSend() {
    await this.buildSessionInStorage()
    const sessionToBeSent = await Storage.get({key: 'session'})
    const session: Session = JSON.parse(sessionToBeSent.value)
    console.log(session)
    // await this.sendSession()
    await this.router.navigateByUrl('tabs/session/available')
  }





  async buildSessionInStorage() {
    // Get previous data in localStorage
    const sessionStorage = await Storage.get({key: 'session'})
    const sessionBefore = JSON.parse(sessionStorage.value)
    const platform = sessionBefore.platform
    const gamename = sessionBefore.game
    const gameId = sessionBefore.gameId
    const mode = sessionBefore.mode
    const goal = sessionBefore.goal
    const date = sessionBefore.date
    const day = 86400000
    let userId: string = null

    // Get current user (uid)
    await this.auth.getCurrentUser().then((res) => {
          userId = res.uid
        },
        async (err) => {
          console.log('Error: ' + err)
          const alert = await this.alertController.create({
            header: 'Erreur lors de l\'obtention de l\'utilisateur courant',
            message: err.message,
            buttons: ['OK'],
          })
          await alert.present()
        })


    // Finish building Session object to be sent
    // uid is generated in backend side
    await Storage.set({
      key: 'session',
      value: JSON.stringify({
        uid: null,
        game: gamename,
        gameId,
        mode,
        goal,
        platform,
        date,
        description: this.descriptionForm.value,
        createdAt: Date.now(),
        expiredAt: (Date.now() + day),
        createdBy: userId,
        listPlayers: [userId]
      })
    })
  }

  private getSessionFromStorage: () => Promise<any[]> = async ()  => {
    return new Promise (async resolve => {
      const sessionStorage = await Storage.get({key: 'session'})
      const session = JSON.parse(sessionStorage.value)

      this.choices.push(session.game, session.mode, session.goal, session.platform, new Date(session.date).toLocaleDateString())

      resolve(this.choices)
    })
  }
}
