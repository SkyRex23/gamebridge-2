import { Component, OnInit, ViewChild } from '@angular/core'
import { Session } from '../../../interfaces/Session'
import { ActivatedRoute, Router } from '@angular/router'
import { AuthService } from '../../../services/auth/auth.service'
import { HttpClient } from '@angular/common/http'
import { Plugins } from '@capacitor/core'
import { User } from '../../../interfaces/User'
import { ChatService } from '../../../services/chat/chat.service'
import { Message } from '../../../interfaces/Message'
import { Observable } from 'rxjs'
const  { Storage } = Plugins

@Component({
    selector: 'app-session-chat',
    templateUrl: './session-chat.page.html',
    styleUrls: ['./session-chat.page.scss'],
})
export class SessionChatPage implements OnInit {
    @ViewChild('content') private content: any

    session: Session = null
    newMsg = ''
    messages: Observable<Message[]>
    myUserUid: string = null

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private auth: AuthService,
        private http: HttpClient,
        private chatService: ChatService,
    ) {
        this.route.queryParams.subscribe(async () => {

            if (this.router.getCurrentNavigation().extras.state) {
                this.session = this.router.getCurrentNavigation().extras.state.session
            } else if (this.router.getCurrentNavigation().extras.state === null) {
                await this.router.navigateByUrl('tabs/homepage')
            } else {
                await this.router.navigateByUrl('tabs/homepage')
            }
        })
    }

    async ngOnInit() {
        this.messages = await this.chatService.getMessages(this.session.uid)
        await this.auth.getCurrentUser().then((user) => this.myUserUid = user.uid)
    }

    ionViewWillEnter() {
        this.scrollToBottomOnInit()
    }

    async sendMessage() {
        const user = await Storage.get({key: 'userLogged'})
        const currentUser: User = JSON.parse(user.value)

        await this.http.post('https://us-central1-gamebridge-app.cloudfunctions.net/addChatMessage', {
            sessionUid: this.session.uid,
            userUid: currentUser.uid,
            msg: this.newMsg,
            username: currentUser.username,
        }, {
            responseType: 'text', headers: {'Access-Control-Allow-Origin': '*'}
        })
            .toPromise()
            .then(() => {
                console.log('Message sent')
                this.newMsg = ''
                this.scrollToBottomOnInit()
            }).catch((err) => {
                this.newMsg = ''
                console.log(err)
                this.scrollToBottomOnInit()
            })
    }

    scrollToBottomOnInit() {
        setTimeout(() => {
            if (this.content.scrollToBottom) {
                this.content.scrollToBottom(0)
                    .then(() => console.log('scrolled'))
                    .catch(err => console.log(err))
            }
        }, 500)
    }
}
