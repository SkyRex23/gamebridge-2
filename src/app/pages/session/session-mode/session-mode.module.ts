import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule } from '@angular/forms'

import { IonicModule } from '@ionic/angular'

import { SessionModePageRoutingModule } from './session-mode-routing.module'

import { SessionModePage } from './session-mode.page'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SessionModePageRoutingModule
  ],
  declarations: [SessionModePage]
})
export class SessionModePageModule {}
