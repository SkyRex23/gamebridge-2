import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { SessionModePage } from './session-mode.page'

const routes: Routes = [
  {
    path: '',
    component: SessionModePage
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SessionModePageRoutingModule {}
