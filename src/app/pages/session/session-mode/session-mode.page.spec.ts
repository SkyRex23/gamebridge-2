import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing'
import { IonicModule } from '@ionic/angular'

import { SessionModePage } from './session-mode.page'

describe('SessionModePage', () => {
  let component: SessionModePage
  let fixture: ComponentFixture<SessionModePage>

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SessionModePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents()

    fixture = TestBed.createComponent(SessionModePage)
    component = fixture.componentInstance
    fixture.detectChanges()
  }))

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
