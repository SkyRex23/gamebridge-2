import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { ProfilePage } from './profile.page'
import {canActivate} from '@angular/fire/auth-guard'

const routes: Routes = [
  {
    path: '',
    component: ProfilePage,
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class ProfilePageRoutingModule {}
