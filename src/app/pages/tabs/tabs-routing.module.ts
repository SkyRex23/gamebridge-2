import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { TabsPage } from './tabs.page'
import { canActivate, redirectLoggedInTo, redirectUnauthorizedTo } from '@angular/fire/auth-guard'

// Send unauthorized users to login
const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['/login'])

// Automatically log in users
// const redirectLoggedInToApp = () => redirectLoggedInTo(['/tabs/homepage'])

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      {
        path: '',
        redirectTo: 'homepage',
        pathMatch: 'full',
      },
      {
        path: 'homepage',
        ...canActivate(redirectUnauthorizedToLogin),
        loadChildren: () => import('../homepage/homepage.module').then(m => m.HomepagePageModule),
      },
      {
        path: 'session/platform',
        ...canActivate(redirectUnauthorizedToLogin),
        loadChildren: () => import('../session/session-platform/session-platform.module').then(m => m.SessionPlatformPageModule),
      },
      {
        path: 'session/goal',
        ...canActivate(redirectUnauthorizedToLogin),
        loadChildren: () => import('../session/session-goal/session-goal.module').then(m => m.SessionGoalPageModule)
      },
      {
        path: 'session/game',
        ...canActivate(redirectUnauthorizedToLogin),
        loadChildren: () => import('../session/session-game/session-game.module').then(m => m.SessionGamePageModule)
      },
      {
        path: 'session/description',
        ...canActivate(redirectUnauthorizedToLogin),
        loadChildren: () => import('../session/session-description/session-description.module').then(m => m.SessionDescriptionPageModule)
      },
      {
        path: 'session/available',
        ...canActivate(redirectUnauthorizedToLogin),
        loadChildren: () => import('../session/session-available/session-available.module').then(m => m.SessionAvailablePageModule)
      },
      {
        path: 'profile/pseudos',
        ...canActivate(redirectUnauthorizedToLogin),
        loadChildren: () => import('../post-signup/account-pseudos/account-pseudos.module').then(m => m.AccountPseudosPageModule)
      },
      {
        path: 'session/date',
        ...canActivate(redirectUnauthorizedToLogin),
        loadChildren: () => import('../session/session-date/session-date.module').then( m => m.SessionDatePageModule)
      },
      {
        path: 'session/mode',
        ...canActivate(redirectUnauthorizedToLogin),
        loadChildren: () => import('../session/session-mode/session-mode.module').then( m => m.SessionModePageModule)
      },
      {
        path: 'session/chat',
        loadChildren: () => import('../session/session-chat/session-chat.module').then( m => m.SessionChatPageModule)
      },
      {
        path: 'profile',
        ...canActivate(redirectUnauthorizedToLogin),
        loadChildren: () => import('../profile/profile.module').then(m => m.ProfilePageModule),
      }
    ]
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule {}
