import { Injectable } from '@angular/core'
import { Message } from '../../interfaces/Message'
import { AngularFirestore } from '@angular/fire/firestore'
import { Observable } from 'rxjs'


@Injectable({
    providedIn: 'root'
})
export class ChatService {

    constructor(private afs: AngularFirestore) {
    }

    getMessages(sessionUid) {
        return this.afs.collection('sessions')
            .doc(sessionUid)
            .collection('messages', ref =>
                ref.orderBy('createdAt')
            ).valueChanges() as Observable<Message[]>
    }
}
