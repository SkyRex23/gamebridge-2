export interface Goal {
    name: string,
    orderPriority: number,
}
