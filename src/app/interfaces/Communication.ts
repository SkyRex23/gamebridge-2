export interface Communication {
    name: string,
    orderPriority: number,
}
