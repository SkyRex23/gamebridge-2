export interface Level {
    name: string,
    orderPriority: number,
}
