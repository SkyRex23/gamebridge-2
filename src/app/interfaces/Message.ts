import Timestamp = firebase.firestore.Timestamp
import * as firebase from 'firebase'

export interface Message {
    msgUid: string,
    msg: string,
    createdByUsername: string,
    createdBy: string,
    createdAt: Timestamp
}
